##Домашнее задание 1

1. Запускаем h2 базу
2. Выполняем в веб интерфейсе скрипты создания таблиц
>   CREATE TABLE EMPLOYEE (
>   ID INT PRIMARY KEY,
>   NAME VARCHAR(255) NOT NULL,
>   LOGIN VARCHAR(50) NOT NULL
>   );
> 
>   CREATE TABLE MESSENGER (
>   ID INT PRIMARY KEY,
>   NAME VARCHAR(255) NOT NULL
>   );
> 
>   CREATE TABLE EMPLOYEE_MESSENGER (
>   EMPLOYEE_ID INT NOT NULL,
>   MESSENGER_ID INT NOT NULL
>   );
> 
>   ALTER TABLE EMPLOYEE_MESSENGER
>   ADD CONSTRAINT EMPLOEYE_MESSENGER_EMPLOYEE_ID_FK
>   FOREIGN KEY (EMPLOYEE_ID)
>   REFERENCES EMPLOYEE(ID);
> 
>   ALTER TABLE EMPLOYEE_MESSENGER
>   ADD CONSTRAINT EMPLOEYE_MESSENGER_MESSENGER_ID_FK
>   FOREIGN KEY (MESSENGER_ID)
>   REFERENCES MESSENGER(ID);

3. Создаем проект и файл liquibase.properties
4. Выполняем команду generateChangeLog в h2.sql
5. Рефакторим полученные скрипты и создаем master-file и папку с release-1.0
6. Пушим ветку release-1.0
7. Делаем новую ветку  release-1.1
8. Создаем новую таблицу с помощью changelog sql
> PRIVELEGE( ID(INT PRIMARY KEY), NAME (VARCHAR(255)))
- Добавляем новое поле privelege_id в таблицу employee и делаем FOREIGN KEY на таблицу Privelege
9. Перезапускаем базу h2 и на DEV накатываем изменения(liquibase update) release-1.0 и release-1.1
10. Делаем ПР из ветки release-1.1 в ветку release-1.0
11. Из ветки release-1.0 ответвляем новую ветку release-2.0
с помощью changelog sql
создаем новую таблицу 
>ISSUE ( ID(INT PRIMARY KEY), NAME (VARCHAR(255)), MESSENGER_ID(INT), ISSUE_TEXT(VARCHAR(255)))
- добавляем  FK на таблицу MESSENGER
12. на INTEGRATION накатываем изменения(liquibase update) release-1.0 и release-2.0
13. Делаем команды diff и diffChangelog в любом формате, добавляем их как файлы в ветку release-2.0
14. Делаем ПР из ветки release-2.0 в ветку release-1.0